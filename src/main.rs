extern crate indicatif;
extern crate serde;
extern crate toml;
extern crate play;

#[macro_use]
extern crate serde_derive;

use std::thread;
use std::time::{Duration, Instant};

use indicatif::{ProgressBar, ProgressStyle};

#[derive(Deserialize)]
struct Config {
    time: TimeConfig,
}

#[derive(Deserialize)]
struct TimeConfig {
    focus: Option<u64>,
    rest: Option<u64>,
}

fn pomodoro() {
    // LOAD TOML FILE
    let config: Config = toml::from_str(r#"
        [time]
        focus = 40
        rest = 10

        [options]
        focus_sound = "focus.mp3"
        rest_sound = "rest.mp3"

    "#).unwrap();

    // VALIDATE DATA
    let focus_time_sec = match config.time.focus {
        Some(value) => value * 60,
        _ => 30 * 60,
    };

    let rest_time_sec = match config.time.rest {
        Some(value) => value * 60,
        _ => 15 * 60,
    };

    let pb = ProgressBar::new(focus_time_sec);
    pb.set_style(ProgressStyle::default_bar().template(""));

    loop {
        pb.set_position(0);
        let sty_yellow = ProgressStyle::default_bar()
            .template(&format!("{{spinner}} - {{prefix:.bold}} |{{wide_bar:.{}}}▏{{percent:.bold}}% {{eta_precise}}", "yellow"))
            .progress_chars("█▓▒░  ");
        pb.set_style(sty_yellow);
        pb.set_prefix("[Focus ]");
        pb.set_length(focus_time_sec);
        let focus_start_mark = Instant::now();

        play::play("assets/audio/t-rex.mp3").unwrap();
        while focus_start_mark.elapsed().as_secs() < focus_time_sec {
            thread::sleep(Duration::from_millis(1000));
            pb.set_position(focus_start_mark.elapsed().as_secs());
        }

        pb.set_position(0);
        let sty_green = ProgressStyle::default_bar()
            .template(&format!("{{spinner}} - {{prefix:.bold}} |{{wide_bar:.{}}}▏{{percent:.bold}}% {{eta_precise}}", "green"))
            .progress_chars("█▓▒░  ");
        pb.set_style(sty_green);
        pb.set_prefix("[Rest  ]");
        pb.set_length(rest_time_sec);

        play::play("assets/audio/puppy.mp3").unwrap();
        let rest_start_mark = Instant::now();
        while rest_start_mark.elapsed().as_secs() < rest_time_sec {
            thread::sleep(Duration::from_millis(1000));
            pb.set_position(rest_start_mark.elapsed().as_secs());
        }
    }
}

fn main() {
    thread::spawn(pomodoro);
    loop {
    }
}


